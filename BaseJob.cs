﻿using System.Threading.Tasks;

namespace Lambdas
{
    public abstract class BaseJob<T> : IJob<T>
    {
        public Task RunAsync(T args)
        {
            return ExecuteAsync(args);
        }

        public abstract Task ExecuteAsync(T args);
    }
}
