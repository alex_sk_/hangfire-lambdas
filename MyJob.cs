﻿using System;
using System.Threading.Tasks;

namespace Lambdas
{
    public class MyJob : BaseJob<MyJobParams>
    {
        public override async Task ExecuteAsync(MyJobParams args)
        {
            Console.WriteLine("My job was perfomed: " + args.Name);

            await Task.Yield();
        }
    }
}
