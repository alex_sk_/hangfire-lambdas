﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lambdas
{
    public class BgService
    {
        public static void Schedule<T>(Expression<Func<T, Task>> methodCall)
        {
            T job = Activator.CreateInstance<T>();
            methodCall.Compile().DynamicInvoke(job);
        }
    }
}
