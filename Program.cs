﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Lambdas
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
            Console.ReadKey();
        }

        static async Task MainAsync(string[] args)
        {
            var job = new MyJob();
            await job.RunAsync(new MyJobParams { Name = "test" });

            CallAsLambda();
            CallAsExpression(typeof(MyJob), new MyJobParams { Name = "test from expression" });
        }

        private static void CallAsLambda()
        {
            BgService.Schedule<MyJob>(j => j.RunAsync(new MyJobParams { Name = "test from lambda" }));
        }

        private static void CallAsExpression(Type jobType, object jobParams)
        {
            MethodInfo method = jobType.GetMethod(nameof(IJob<object>.RunAsync));

            var x = Expression.Parameter(jobType, "x");

            var methodCall = Expression.Call(x, method, Expression.Constant(jobParams));

            var expression = Expression.Call(
                typeof(BgService),
                nameof(BgService.Schedule),
                new Type[] { jobType },
                new Expression[]
                {
                    Expression.Lambda(methodCall, x)
                });

            Expression.Lambda(expression).Compile().DynamicInvoke();
        }
    }
}
