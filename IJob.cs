﻿using System.Threading.Tasks;

namespace Lambdas
{
    public interface IJob<T>
    {
        Task RunAsync(T args);
    }
}